/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package prgt1e1;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Paco Aldarias
 */
public class Prgt1e1Test {
    static Prgt1e1 ceed_t1e1;
    static  double res;
    
   
    @BeforeClass
    public static void setUpClass() {
        ceed_t1e1 = new Prgt1e1 ();
        
    }
  
    /**
     * Test of main method, of class Prgt1e1.
     */
    @Test
    public void testMain() {
                
        System.out.println("main");
        String[] args = null;
        Prgt1e1.main(args);
        res = Prgt1e1.c;
        System.out.println("Test Suma "+res);
        Assert.assertEquals ( "Test ", 3.0 , res , 1e-6);

    }
}